<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SessionsController extends Controller
{
    public function login()
    {
        return view("porto.security.login");
    }

    public function postLogin(Request $request)
    {
        if(Auth::attempt([
            'name' => $request->input("name"),
            'password' => $request->input("password"),
            'verified' => true
        ]))
        {
            Auth::user()->updateLastLogin();
            // ADD Log Event dedicated to User
            return redirect()->intended("/");
        }
        $message = "Echec de la connexion";
        session()->flash("message", $message);
        return redirect("login");
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->back();
    }
}

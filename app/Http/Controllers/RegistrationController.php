<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Mailers\AppMailer;

class RegistrationController extends Controller
{
    public function register()
    {
        return view("porto.users.register");
    }

    public function postRegister(Request $request, AppMailer $mailer)
    {
        $this->validate($request, [
            "name" => 'required|unique:users',
            "email" => "required|email",
            "password" => "required|confirmed"
        ]);
        $user = User::create($request->all());
        $mailer->sendEmailConfirmationTo($user);
        $message = "Merci de confirmer votre adresse email";
        session()->flash("message", $message);
        return redirect()->back();
    }

    public function confirm($token)
    {
        User::where("email_token",$token)->firstOrFail()->confirmEmail();
        $message = "Email vérifiée. Vous pouvez vous connecter :)";
        session()->flash("message", $message);
        return redirect("login");
    }
}

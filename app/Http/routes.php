<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('inscription', ['uses' => "RegistrationController@register", 'as' => "inscription"]);
    Route::post('inscription', ['uses' => "RegistrationController@postRegister", 'as' => "inscription_post"]);
    Route::get('inscription/confirm/{token}', ['uses' => "RegistrationController@confirm", 'as' => "inscription_verify"]);

    Route::get('login', ['uses' => "SessionsController@login", 'as' => "login"]);
    Route::post('login', ['uses' => "SessionsController@postLogin", 'as' => "login_post"]);
    Route::get('logout', ['uses' => "SessionsController@logout", 'as' => "logout"]);

    Route::get("oubli", ['uses' => "RemindController@ask", "as" => "password_ask"]);
    Route::post("oubli", ['uses' => "RemindController@send", 'as' => "password_send"]);
    Route::post("/oubli/mod", ["uses" => "RemindController@store", "as" => "password_do"]);
    Route::get("/oubli/{token}", ["uses" => "RemindController@change", 'as' => "password_change"]);
});

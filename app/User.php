<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use App\SlugTrait;

class User extends Authenticatable
{
    use SlugTrait;

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function($user){
            $user->email_token = str_random(30);
        });
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function confirmEmail()
    {
        $this->verified = true;
        $this->email_token = null;
        $this->save();
    }

    public function updateLastLogin()
    {
        $this->last_login = new \DateTime();
        $this->save();
    }
}

@extends("porto.layout.simple")

@section('body') fond ilum @stop
@section("page")
<section class="body-sign">
    <div class="center-sign">
        <a href="/" class="logo pull-left">
            <img src="{{ asset("assets/images/logo-black.png") }}" height="54" alt="SWOR" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Connexion</h2>
            </div>
            <div class="panel-body">
                {{ Form::open(['action' => 'SessionsController@postLogin', "method" => "POST"]) }}
                <!-- if there are login errors, show them here -->
                @include("porto.partials.success")
                @include("porto.partials.message-flash")
                @include("porto.partials.errors")
                <div class="form-group mb-lg">
                    <label for="username">Username</label>
                    <div class="input-group input-group-icon">
                        <input name="name" type="text" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-user"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Password</label>
                            <a href="{{ URL::to("oubli") }}" class="pull-right">Mot de passe perdu ?</a>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" type="password" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Se souvenir de moi</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">La Force est avec moi</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">La Force est avec moi</button>
                        </div>
                    </div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

                    <p class="text-center">Pas encore de compte ? <a href="{{ URL::route("inscription") }}">Créer un compte</a>

                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>
@stop
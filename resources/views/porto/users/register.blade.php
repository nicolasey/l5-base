@extends("porto.layout.simple")

@section("body") fond coruscant-night @stop

@section("page")
    <section class="body-sign">
        <div class="center-sign">
            <a href="/" class="logo pull-left">
                <img src="{{ asset("assets/images/logo-jaune.png") }}" height="54" alt="SWOR" />
            </a>

            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Connexion</h2>
                </div>
                <div class="panel-body">
                    {{ Form::open(['url' => 'inscription']) }}
                    <!-- if there are login errors, show them here -->
                    @include("porto.partials.success")
                    @include("porto.partials.message-flash")
                    @include("porto.partials.errors")
                    <div class="form-group mb-lg">
                        <label for="name">Username</label>
                        <div class="input-group input-group-icon">
                            <input name="name" type="text" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-user"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <label for="username">Email</label>
                        <div class="input-group input-group-icon">
                            <input name="email" type="email" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-at"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Password</label>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" type="password" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Confirmer le Password</label>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password_confirmation" type="password" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <p>&nbsp;</p>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">La Force est avec moi</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">La Force est avec moi</button>
                        </div>
                    </div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

                    <p class="text-center">Déjà un compte ? <a href="{{ URL::route("login") }}">Se connecter</a>

                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop
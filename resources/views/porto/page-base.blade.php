@extends("porto.layout.main")

@section("page")
<section role="main" class="content-body">
    <header class="page-header">
        <h2>Menu Collapsed Layout</h2>

        <div class="right-wrapper pull-right">
            <ol class="breadcrumbs">
                <li>
                    <a href="index.html">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li><span>Layouts</span></li>
                <li><span>Menu Collapsed</span></li>
            </ol>

            <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
        </div>
    </header>

    <!-- start: page -->

    <!-- end: page -->
</section>
@stop
@extends("porto.layout.simple")

@section("body") fond kuat @stop

@section("page")
    <section class="body-sign">
        <div class="center-sign">
            <a href="/" class="logo pull-left">
                <img src="{{ asset("assets/images/logo-black.png") }}" height="54" alt="SWOR" />
            </a>

            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Petit oubli</h2>
                </div>
                <div class="panel-body">
                    {{ Form::open(array('route' => 'password_send')) }}
                    <!-- if there are login errors, show them here -->
                    <p>
                        @if (Session::has('error'))
                            {{ trans(Session::get('reason')) }}
                        @elseif (Session::has('success'))
                            An email with the password reset has been sent.
                        @endif
                    </p>
                    <div class="form-group mb-lg">
                        <label for="username">Email</label>
                        <div class="input-group input-group-icon">
                            <input name="email" type="email" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-at"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">La Force est avec moi</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">La Force est avec moi</button>
                        </div>
                    </div>

							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>ou</span>
							</span>

                    <p class="text-center">Pas encore de compte ? <a href="{{ URL::route("inscription") }}">Créer un compte</a>

                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop
@extends("porto.layout.simple")

@section("body") fond alderaan @stop

@section("page")
    <section class="body-sign">
        <div class="center-sign">
            <a href="/" class="logo pull-left">
                <img src="{{ asset("assets/images/logo-black.png") }}" height="54" alt="SWOR" />
            </a>

            <div class="panel panel-sign">
                <div class="panel-title-sign mt-xl text-right">
                    <h2 class="title text-uppercase text-weight-bold m-none"><i class="fa fa-user mr-xs"></i> Connexion</h2>
                </div>
                <div class="panel-body">
                    {{ Form::open(array('route' => array('password.update', $token))) }}
                    <!-- if there are login errors, show them here -->
                    <p>
                        @if (Session::has('error'))
                            {{ trans(Session::get('reason')) }}
                        @endif
                    </p>
                    <div class="form-group mb-lg">
                        <label for="username">Email</label>
                        <div class="input-group input-group-icon">
                            <input name="email" type="email" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-at"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Password</label>
                            <a href="{{ URL::to("password/reset") }}" class="pull-right">Mot de passe perdu ?</a>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" type="password" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Confirmation du Password</label>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password_confirmation" type="password" class="form-control input-lg" />
                            <span class="input-group-addon">
                                <span class="icon icon-lg">
                                    <i class="fa fa-lock"></i>
                                </span>
                            </span>
                        </div>
                    </div>
                    {{ Form::hidden('token', $token) }}
                    <div class="row">
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">La Force est avec moi</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">La Force est avec moi</button>
                        </div>
                    </div>

                        {{ Form::close() }}
                </div>
            </div>
        </div>
    </section>
@stop
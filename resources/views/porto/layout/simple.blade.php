<!doctype html>
<html class="fixed sidebar-left-collapsed" lang="fr" xml:lang="fr">
<head>
    <meta charset="UTF-8">
    <title>@section("head_title") StarWars Open Rift @show</title>
    <meta name="keywords" content="HTML5 Admin Template" />
    <meta name="description" content="Porto Admin - Responsive HTML5 Template">
    <meta name="author" content="SWOR jdr">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap/dist/css/bootstrap.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("bower_components/font-awesome/css/font-awesome.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("bower_components/magnific-popup/dist/magnific-popup.css") }}" />
    <link rel="stylesheet" href="{{ asset("bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.min.css") }}" />

    <link rel="stylesheet" href="{{ asset("assets/stylesheets/theme.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/stylesheets/skins/default.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/stylesheets/theme-custom.css") }}">
    @yield("css")

    <script src="{{ asset("bower_components/modernizr/src/Modernizr.js") }}"></script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-62372292-2', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body class="@yield('body')">

    @yield("page")

    <script src="{{ asset("bower_components/jquery/dist/jquery.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js") }}"></script>
    <script src="{{ asset("bower_components/bootstrap/dist/js/bootstrap.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("bower_components/nanoscroller/bin/javascripts/jquery.nanoscroller.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("bower_components/magnific-popup/dist/jquery.magnific-popup.min.js") }}" type="text/javascript"></script>
    <script src="{{ asset("bower_components/jquery-placeholder/jquery.placeholder.min.js") }}" type="text/javascript"></script>
    @yield("js")

    <script src="{{ asset("assets/javascripts/theme.js") }}"></script>
    <script src="{{ asset("assets/javascripts/theme.custom.js") }}"></script>
    <script src="{{ asset("assets/javascripts/theme.init.js") }}"></script>
</body>
</html>
<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Confirmation de votre Email</title>
</head>
<body>
<h1>Merci de vous être inscrit(e) !</h1>
<p>
    Pour débuter l'aventure, <a href="{{ url("inscription/confirm/{$user->email_token}") }}">confirmez votre adresse email</a>
</p>
</body>
</html>